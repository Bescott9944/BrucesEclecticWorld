#!/bin/bash

#####################################################################
# Script Name   : 991-samba-cfg.sh
# Description   : Install my programs, configs and scripts from my gitlab repo for my system after install/re-install
# Dependencies  : None
# Arguments     : Version: -v, Help: -h
# Author        : Bruce E. Scott, 21 June 2022
# Started On    : 10 June 2022 12:44 p.m. EST 2022 v.0.3 Beta
# Last Change   : 21 June 2022 11:15 p.m. EST 2022
# Author E-Mail : bescott9944@gmail.com
# Author GitHub : https://github.com/bescott9944
# Auther Gitlab : https://gitlab.com/bescott9944
# Linux Forms   : I can be reached at https://www.ezeelinux.com/talk/index.php (bescott9944)
# Website		: https://www.stargazertwo.com
###################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################
#tput setaf 0 = black
#tput setaf 1 = red
#tput setaf 2 = green
#tput setaf 3 = yellow
#tput setaf 4 = dark blue
#tput setaf 5 = purple
#tput setaf 6 = cyan
#tput setaf 7 = gray
#tput setaf 8 = light blue
##################################################################################################################

#This Help/Version/Error script section came from TerminalForLife.
#GitHub:https://github.com/terminalforlife
CurVer=' v.1.3.1 2022-05-24 '
Progrm=${0##*/}

Err(){
	printf 'ERROR: %s\n' "$2" 1>&2
	[ $1 -gt 0 ] && exit $1

}

USAGE(){
	while read; do
		printf "%s\n" "$REPLY"
	done <<-EOF
		            ${Progrm^^} ($CurVer)
		            Written by Bruce E. Scott <bescott9944@gmail.com>

		            Simple Script to install all my system configs.

		SYNTAX:     $_PROJECT_ [OPTS]

		OPTS:       --help|-h|-?            - Displays this help information.
		            --version|-v            - Output only the version datestamp.
		            --debug|-D              - Enables the built-in bash debugging.
		            
		            

		NOTE:       The purpose of the script is to install my configs files
		and scrips for a system install or re-install. --Bruce E. Scott
	EOF
}

while [ "$1" ]; do
	case $1 in
		--help|-h|-\?)
			USAGE; exit 0 ;;
		--version|-v)
			printf '%s\n' "$CurVer"; exit 0 ;;
		--debug|-D)
			DEBUGME='true' ;;
		
		-*)
			Err 1 $LINENO "Incorrect argument(s) specified." ;;
		*)
			break ;;
	esac
	shift
done

#-------------------------------------------------------------------#

# Set BASH to quit script and exit on any errors:
 #Added this to the script on 06/20/2022.. -Bruce
 set -e pipefail

#-------------------------------------------------------------------#

# when on Arch Linux
  installed_dir=$(dirname $(readlink -f $(basename `pwd`)))

#   grep -q "Arch Linux" /etc/os-release; then
#		[ -d "/etc/samba" ] || mkdir -p "/etc/samba"

  echo
  tput setaf 2
  echo "################################################################"
  echo "######### Installing Samba Software for Arch Linux - Any desktop"
  echo "################################################################"
  tput sgr0
  echo Done
  echo
  echo
  sleep 2


### $HOME/Stargazer-Drive
    [ -d $HOME"/Stargazer-Drive" ] || mkdir -p $HOME"/Stargazer-Drive"
  echo
  tput setaf 2
  echo "################################################################"
  echo "################### Done Creating $HOME/Stargazer-Drive"
  echo "################################################################"
  tput sgr0
  echo
  echo Done
  echo
  echo
  sleep 2

### /etc/samba/smb.conf
  echo
  tput setaf 2
  echo "################################################################"
  echo "################### Copying smb.config to /etc/samba"
  echo "################################################################"
  tput sgr0
  echo
  echo Done
  echo
  echo
  sleep 1

  if [ -f "/etc/samba/smb.conf" ];then

		sudo cp /etc/samba/smb.conf /etc/samba/smb.conf.bak
 fi

  if [ -d "/etc/samba" ];then
  
		sudo cp $installed_dir/settings/etc/samba/smb.conf* /etc/samba/
fi
        #create a group sambashare…
if grep -q "sambashare" /etc/group; then

  echo
  tput setaf 2
  echo "################################################################"
  echo "################### Already A Sambashare Goup"
  echo "################################################################"
  tput sgr0
  echo
       
  else
        sudo groupadd -r sambashare

fi

        #create usershare folder…
        sudo mkdir -p /var/lib/samba/usershare

        #change group of usershare folder…
        sudo chown root:sambashare /var/lib/samba/usershare

        #change mode w/sticky bit on usershare folder…
        sudo chmod 1770 /var/lib/samba/usershare

        #add your user to samba (then create new sharing password)…
        sudo smbpasswd -a bruce

        #then add you user to sambashare group:
        sudo gpasswd -a bruce sambashare

        #now start and enable (to get it started automatic on boot)
        sudo systemctl start smb nmb

        sudo systemctl enable smb nmb
        #Now you can use system-config-samba!


echo
tput setaf 2
echo "################################################################"
echo "################### Done Installing Samba .Conf Software"
echo "################################################################"
tput sgr0
echo
echo Done
sleep 3

