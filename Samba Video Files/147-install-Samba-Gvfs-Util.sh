#!/bin/bash

#####################################################################
# Script Name   : 147-install-Samba-Gvfs-Util.sh
# Description   : Install my programs, configs and scripts from my gitlab repo for my system after install/re-install
# Dependencies  : None
# Arguments     : Version: -v, Help: -h
# Author        : Bruce E. Scott, 21 June 2022
# Started On    : 10 June 2022 12:44 p.m. EST 2022 v.0.3 Beta
# Last Change   : 21 June 2022 11:15 p.m. EST 2022
# Author E-Mail : bescott9944@gmail.com
# Author GitHub : https://github.com/bescott9944
# Auther Gitlab : https://gitlab.com/bescott9944
# Linux Forms   : I can be reached at https://www.ezeelinux.com/talk/index.php (bescott9944)
# Website		: https://www.stargazertwo.com
###################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################
#tput setaf 0 = black
#tput setaf 1 = red
#tput setaf 2 = green
#tput setaf 3 = yellow
#tput setaf 4 = dark blue
#tput setaf 5 = purple
#tput setaf 6 = cyan
#tput setaf 7 = gray
#tput setaf 8 = light blue
##################################################################################################################
##### Designer of 0-current-choices.sh
# Author    : Erik Dubois
# Website   : https://www.erikdubois.be
# Website   : https://www.alci.online
# Website   : https://www.ariser.eu
# Website   : https://www.arcolinux.info
# Website   : https://www.arcolinux.com
# Website   : https://www.arcolinuxd.com
# Website   : https://www.arcolinuxb.com
# Website   : https://www.arcolinuxiso.com
# Website   : https://www.arcolinuxforum.com
##################################################################################################################

#This Help/Version/Error script section came from TerminalForLife.
#GitHub:https://github.com/terminalforlife
CurVer=' v.1.3.1 2022-05-24 '
Progrm=${0##*/}

Err(){
	printf 'ERROR: %s\n' "$2" 1>&2
	[ $1 -gt 0 ] && exit $1

}

USAGE(){
	while read; do
		printf "%s\n" "$REPLY"
	done <<-EOF
		            ${Progrm^^} ($CurVer)
		            Written by Bruce E. Scott <bescott9944@gmail.com>

		            Simple Script to install all my system configs.

		SYNTAX:     $_PROJECT_ [OPTS]

		OPTS:       --help|-h|-?            - Displays this help information.
		            --version|-v            - Output only the version datestamp.
		            --debug|-D              - Enables the built-in bash debugging.
		            
		            

		NOTE:       The purpose of the script is to install my configs files
		and scrips for a system install or re-install. --Bruce E. Scott
	EOF
}

while [ "$1" ]; do
	case $1 in
		--help|-h|-\?)
			USAGE; exit 0 ;;
		--version|-v)
			printf '%s\n' "$CurVer"; exit 0 ;;
		--debug|-D)
			DEBUGME='true' ;;
		
		-*)
			Err 1 $LINENO "Incorrect argument(s) specified." ;;
		*)
			break ;;
	esac
	shift
done

#-------------------------------------------------------------------#

# Set BASH to quit script and exit on any errors:
 #Added this to the script on 06/20/2022.. -Bruce
 set -e pipefail

#-------------------------------------------------------------------#

tput setaf 2
echo "################################################################"
echo "############# Installing Gvfs-Samba Util's"
echo "################################################################"
tput sgr0
echo
echo
sleep 2

#sudo pacman --needed -Sy
echo -e "\nInstalling Gvfs-Samba Util's\n"

PKGS=(

# Gvfs-Samba Util's:
'gvfs-afc'
'gvfs-goa'
'gvfs-google'
'gvfs-gphoto2'
'gvfs-mtp'
'gvfs-nfs'
'gvfs-smb'

)

for PKG in "${PKGS[@]}"; do
    echo "INSTALLING: ${PKG}"
    sudo pacman -S "$PKG" --noconfirm --needed
done

tput setaf 3
echo "################################################################"
echo "############# Installed Gvfs-Samba Util's"
echo "################################################################"
tput sgr0
echo
echo
sleep 2
