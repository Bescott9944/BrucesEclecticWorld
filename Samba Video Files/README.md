## Welcome to my Repo for My Samba Install Method & Script.

### (Use At Your Own Risk)

## The purpose of this Repo:

. This is our method of installing Samba on a Arch or Debian Linux Distro. I found the Samba.txt Text File on the internet back in 2019, most likely on Ask-Ubuntu.
. The SMB.Conf is a combination of my old SMB.conf from my Windows NT Server days and one I found for Linux. I tooked them both over and combined the to make one. The config file has work for years with a bit of tweaking because of the protocol changes.

. The script is part of my reinstall/install scripts to add all my settings to my new install of Arch or some Debian OS. You will have to modify it to work on your system because it is beyond the scope of this video! The script will add all the settings and permissions to Linux so samba will function. It should be easy to read. Good Luck...

. Edited 02-01-2023: I for got this script "147-install-Samba-Gvfs-Util.sh".. On Arch at least, these file are needed to be installed. Most are already so be sure to run this script before the other one.

Please check the code and understand what it is doing! DON"T just blindly run code! YOU HAVE BEEN WARNED!

These will be scripts I use to reinstall or building a new system.
Here you will find some of my Dot Files and Scripts I use for my systems...
I also use these on my Linux Mint, my BTW Arch, MX 19 and others when I get a chance...

<p>Note: If you use any of these you may have to install other files or dependencies
that I use and you don't have. You will also have to edit the file's to work on you system! They Will NOT WORK
Out of the BOX. You HAVE BEEN TOLD!
...

## Files and Scripts:

Some of these files and scripts were borrowed from other sources and the internet. I do NOT clam to those file author, I just found them and edited them for my use...  I don't remember where I got them all but here they are! I know some of the script might not be worded right and I am not a programmer by any means but they work for me.
Edit them as you wish... Make then work for you!
I MAY included some of my ~/bin files for public use that were used in a video.
Also my Polybar, Rofi, and other files, my config file, Themes build files.. Enjoy!
...

## Dependencies:

Please install all the Dependencies and programs needed for things to work! That is up to YOU to do and NOT for me to explain!
That is it! Have fun!
