conky.config = {
-- 
-- Name: Metro Style Conky

-- Author: Kant-o (feedback via gnome-look).

-- Description
-- This simple conky configuration shows the following information:
-- conky.conf.11 - Time and date information;
-- conky.conf.21 - Operative System and Battery information
-- conky.conf.12 - Processes information;
-- conky.conf.31 - Processor and RAM information;
-- conky.conf.32 - FileSystem information;
-- conky.conf.41 - Network information;
-- conky.conf.42 - GMAIL account information;

-- Apart from the requirement for the Ubuntu font, this configuration is
-- really simple, using only a few conky objects and a script provided
-- with the configuration file. The aforementioned script (see mail.sh) is
-- used to get the number of unread mails from google via pipelining several
-- comands and using CURL.

-- Installation (see below for terminal installation)
-- To install and use, the following 5 main steps are required:
-- 1. Install conky and the Ubuntu font (if using recent Ubuntu (based) 
--    distributions, this step is most certainly not necessary.
-- 2. Create a directory named .conky in your home folder;
-- 3. Download the .zip file and unzip the contents to the newly 
--    created folder;
-- 4. Edit the file mail.sh and place your gmail credentials (otherwise it #    will not fetch the number of mails in the gmail account).
-- 5. Run conky.sh (if it does not run, make sure it is executable, 
--    otherwise type chmod +x ~.conky/conky.sh in the terminal before
--    running the script).

-- If you want conky to startup with gnome, copy the file named
-- conky.sh.desktop to ~.config/autostart/
-- If you do not want a given module to run (e.g. the GMAIL module),
-- edit the conky.sh file and comment or remove the line that loads the
-- respective configuration file (in the case of GMAIL, that would be
-- conky.conf.24 - line 2 - column 4).

-- To install using the terminal, follow the next steps
-- 1. In Ubuntu (I have not tested the config in Ubuntu, only in Arch)
--    sudo apt-get install conky-all
--    In Arch Linux
--    sudo yaourt -S conky-lua ttf-ubuntu-font-family 
-- 2. mkdir ~/.conky && cd ~/.conky
-- 3. wget http://www.deviantart.com/download/260230653/metro_style_conky_by_kant_o-d4axn99.zip
-- 4. unzip MetroStyleConky.zip
-- 5. chmod +x conky.sh
-- 6. chmod +x mail.sh
-- 7. ./conky.sh   
-- 8. (optional) run gnome-session-properties and add a new 
--    startup program pointing to conky.sh to have the conky
--    automatically comming up when you start the computer. 


	alignment = 'bottom_right',
	background = true,
	border_width = 1,
	cpu_avg_samples = 1,
	default_color = '#ffffff',
	draw_outline = false,
	draw_borders = true,
	draw_shades = false,
	use_xft = true,
-- xftfont Ubuntu:size=12
	font = 'Bitstream Vera Sans Mono:size=5.8',
-- size of text area
	minimum_width = 275, minimum_height = 90,
	maximum_width = 380,
	max_text_width = 380,

	no_buffers = true,
	out_to_console = false,
	out_to_stderr = false,
	extra_newline = false,
	own_window = true,
	own_window_transparent = true,
	own_window_class = 'Conky',
	own_window_type = 'normal',
	own_window_hints = 'undecorated,sticky,skip_taskbar,skip_pager,below',
	stippled_borders = 0,
	update_interval = 1.0,
	uppercase = false,
	use_spacer = 'none',
	show_graph_scale = false,
	show_graph_range = false,

-- Use double buffering (reduces flicker, may not work for everyone)
	double_buffer = true,

	own_window_colour = '136659',
	update_interval = 5,

	gap_x = 5,
	gap_y = 35,
	own_window_argb_value = 0,
	own_window_argb_visual = false,

--  --Colours

-- default_color D9DDE2  # -- (white-gray) default color and border color
-- default_color 00BFFF # --Mid-light-blue_(gray-blue) default color and border color
	default_color = '#FFFFFF',-- --white default color and border color
	color1 = '#FF0000',-- --red
	color2 = '#597AA1',-- --blue-gray
	color3 = '#cccccc',-- --light-gray
	color4 = '#D9BC83',-- --shade-of-gold
	color5 = '#00BFFF',-- --Mid-light-blue_(gray-blue)
	color6 = '#FFFFFF',-- --white
--	--Signal Colours
	color7 = '#1F7411',-- --green
	color8 = '#FFA726',-- --orange
	color9 = '#F1544B',-- --firebrick
--   --Aditional Colors to Use
-- color DAE140 #  --Golden Yellow
-- color F5FF03 #  --Yellow
-- color 00d8ff #  --Pale Blue
-- color FF6D00 # --Burnt range

};

conky.text = [[
${color yellow}${font Roboto:size=7}N E T W O R K   ${hr 2}${font}${color}
${color6}${offset 30}IP Address: ${color6} ${alignr}${offset -10$}${color8}${addrs wlan0}
${color6}${offset 30}Eth Up:${color6} ${alignr}${offset -10$}${upspeed wlan0}${alignr}${color FF6D00}${upspeedgraph wlan0 8,100}
${color6}${offset 30}Eth Down:${color6} ${alignr}${offset -10$}${downspeed wlan0}${alignr}${color 00d8ff}${downspeedgraph wlan0 8,100}
${color6}${offset 30}Total Dl :${color 00d8ff}${totaldown wlan0} ${alignr}${offset -30}${color6}Total UL: ${alignr}${offset -30}${color FF6D00}${totalup wlan0}
]];
