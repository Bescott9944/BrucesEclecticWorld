## Welcome to my Repo for My Hal & Sal 9000 Computers.

### (Use At Your Own Risk)

The purpose of this Repo is for reinstalling my system file and files after a reinstall or building a new system.
Here you will find some of my Dot Files and Scripts I use for my Hal & Sal setup that I use on my EndeavourOS Arch install...
I also use these on my Linux Mint, my BTW Arch, MX 19 and others when I get a chance...

Note, If you use any of these files you may have to install files or dependencies
that I use and you don't have. You will also have to edit the Conky's to work on you system! They Will NOT WORK
Out of the BOX. You HAVE BEEN TOLD!
...

Also note that All the image file were downloaded from the internet and I edited them to make my Screens. You may use them as you like!
I do not clame any of the work by others. Thanks you!

...

Some of these files were borrowed from other sources and the internet. I don't remember where I got them all
but here they are! I know some of the script might not be worded right and I am not a programmer by any-means but they work for me.
Edit them as you wish... Make then work for you!
I MAY included some of my ~/bin files for public use. Also my Polybar, Rofi, and other files, my config file, Themes build files.. Enjoy!

So that is some of the things I had fun with and yes it was a adventure! Lol

Please install all the Dependencies and programs, Themes. Most of all follow the instruction on the Themes to make the Links to the folders.
That is it! Have fun!

![Screenshot of Hal-9000 in Desktop](screenshots/Hal_desktop.jpg)

![Screenshot of Sal-9000 in Desktop](screenshots/Sal_desktop.jpg)

![Screenshot of Hal-9000 in Log-In](screenshots/Hal_login.jpg)

![Screenshot of Sal-9000 in Log-In](screenshots/Sal_login.jpg)



