# Change-log.md
      _                                           
     /  |_   _. ._   _   _  |  _   _    ._ _   _| 
     \_ | | (_| | | (_| (/_ | (_) (_| o | | | (_| 
                     _|            _|             


- Lead Developer: Bruce E. Scott
- Script Creator: Bruce E. Scott
- GitLab: https://gitlab.com/bescott9944
- Forums: https://forum.endeavouros.com/bescott9944
- Licensed under the GNU and MIT respectively

####   <<<---================}[ Bruce's Eclectic World ]{================--->>>

### -----------------------------------------------------

#### Current Version: None Modified by Bruce Scott 05/23/2022
* Project Name  : Bruce's Eclectic World
* Description   : All my Scripts and files needed to reinstall Hal
* Author        : Bruce E. Scott, May 31 2022
* Started On    : 31 May 2022
* Last Change   : 02 June 01:15 a.m. EST 2022
* Author Email : bescott9944@gmail.com
* Author GitHub : https://github.com/bescott9944
* Author Gitlab : https://gitlab.com/bescott9944
* Linux Forms   : I can be reached at https://forum.endeavouros.com/bescott9944 (bescott9944)
### -----------------------------------------------------


#### May 31th 2022
Setup the Repo to upload all the scripts for my builds of Hal $ Sal 9000 computers.
This could includes all of my /bin, /.config, /.conky, and other misc files needed.

### -----------------------------------------------------

#### Change log. 06/02/2022
- Added my updated Readme.MD. Added some file too.
- This is also kinda testing everything too. -Bruce
### -----------------------------------------------------

#### Change log. 06/02/2022
- Well we spent several hours uploading all the files.
- Sure is lots of fun that is for sure! All my file are there for you to use
- if you want. Just remember they will have to be adjusted to be used on your
- system. Use at your OWN Risk. You have been WARNED!!!
### -----------------------------------------------------

#### Change log. 02/27/2023
- uploaded the current updated verson of Syncall_Starfleet
-which is Syncall_Starfleet_v0.5.5.... -Bruce
### -----------------------------------------------------

### -----------------------------------------------------

### -----------------------------------------------------

### -----------------------------------------------------
